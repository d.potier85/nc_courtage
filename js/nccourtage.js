$(function (){

    //menu mobile
    $('.open-menu').click(function(e){
        $('.menu-mobile').slideDown();
        e.preventDefault();
    });

    $('.close-menu').click(function(e){
        $('.menu-mobile').slideUp();
        e.preventDefault();
    });

    $('.menu-mobile ul').onePageNav({
        end : function (){
            $('.menu-mobile').slideUp();
        }
    });

    //one page nav
    $('#menu').onePageNav();

    //scroll metier
    $('.go').click(function (){
       var elem = $('#metier');
       $('html, body').animate({scrollTop : elem.offsetTop()}, 750);
    });



});
